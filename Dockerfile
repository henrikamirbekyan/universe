FROM nginx
LABEL maintainer="henrikamirbekyan@gmail.com"
WORKDIR /usr/share/nginx/html
COPY . /usr/share/nginx/html
